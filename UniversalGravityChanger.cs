﻿using GTA;
using System;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace UniversalGravityChanger
{
    public class UniversalGravityChanger : Script
    {
        bool gravityEnabled = true, useHoldKey;
        Keys holdkey, presskey;

        public UniversalGravityChanger()
        {
            if (!File.Exists(Settings.Filename))
            {
                Settings.SetValue("hold_key", "keys", Keys.RControlKey);
                Settings.SetValue("use_hold_key", "keys", true);
                Settings.SetValue("press_key", "keys", Keys.G);
                Settings.Save();
            }

            holdkey = Settings.GetValueKey("hold_key", "keys", Keys.RControlKey);
            useHoldKey = Settings.GetValueBool("use_hold_key", "keys", true);
            presskey = Settings.GetValueKey("press_key", "keys", Keys.G);

            KeyDown += UniversalGravityChanger_KeyDown;
            Interval = 50;
            Tick += UniversalGravityChanger_Tick;

        }

        private void UniversalGravityChanger_Tick(object sender, System.EventArgs e)
        {
            if (!gravityEnabled)
                foreach (Vehicle veh in World.GetVehicles(Player.Character.Position, 100.0f))
                    if (!veh.isOnAllWheels) veh.ApplyForce(new Vector3(0, 0, 0.61f));
        }

        private void UniversalGravityChanger_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if ((!useHoldKey || isKeyPressed(holdkey)) && e.Key == presskey)
            {
                World.GravityEnabled = (gravityEnabled = !gravityEnabled);
                string display = "Gravity is now " + (gravityEnabled ? "enabled" : "disabled");
                Game.DisplayText(display);
                Game.Console.Print("Universal Gravity Changer: " + display);
            }
        }
    }
}
