What is this?
The name says it all: it is a simple mod that changes the gravity of everything, including cars.

Why did you make this?
It was requested.

Why did you make this when Simple Trainer has (part of) this already?
Not everyone uses Simple Trainer.

There is already a standalone gravity changer mod... why make another?
The only gravity changer mod I found was made in LUA, so I made it for something more widely used (.NET :P). 
This one also changes the gravity of vehicles, which others don't.

How do I install it?
First install .NET Scripthook, then copy the UniversalGravityChanger.net.dll and UniversalGravityChanger.ini files to the scripts folder in your GTA IV directory.

How do I use it?
Press RCtrl + G together to toggle gravity.
Change the key combo in the ini file.

Changelog:
v1.1:
Now vehicles are affected by gravity too!

Notes:
If there are many vehicles around you, your game may lag (how much depends on the specs of your PC).
Only vehicles within 100m of you are affected by the gravity change (this is not a bug. This is to prevent too much lag.)

Credits:
Aquilon96 for the request.
LetsPlayOrDy (now named Jitnaught) for the script.

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
